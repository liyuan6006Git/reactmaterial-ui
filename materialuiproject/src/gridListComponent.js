import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import GridList, { GridListTile } from 'material-ui/GridList';

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gridList: {
    width: 500,
    height: 450,
  },
  subheader: {
    width: '100%',
  },
});

const tileData = [
    {
      img: 'images/grid-list/00-52-29-429_640.jpg',
      title: 'Breakfast',
      author: 'jill111',
      cols:3
    },
    {
      img: 'images/grid-list/burger-827309_640.jpg',
      title: 'Tasty burger',
      author: 'pashminu',
    },
    {
      img: 'images/grid-list/gold.jpg',
      title: 'Camera',
      author: 'Danson67',
    },
    {
      img: 'images/grid-list/morning-819362_640.jpg',
      title: 'Morning',
      author: 'fancycrave1',
    },
    {
      img: 'images/grid-list/hats-829509_640.jpg',
      title: 'Hats',
      author: 'Hans',
    },
    {
      img: 'images/grid-list/honey-823614_640.jpg',
      title: 'Honey',
      author: 'fancycravel',
    },
    {
      img: 'images/grid-list/vegetables-790022_640.jpg',
      title: 'Vegetables',
      author: 'jill111',
    },
    {
      img: 'images/grid-list/water-plant-821293_640.jpg',
      title: 'Water plant',
      author: 'BkrmadtyaKarki',
    },
  ];
function ImageGridList(props) {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <GridList cellHeight={160} className={classes.gridList} cols={3}>
        {tileData.map(tile => (
          <GridListTile key={tile.img} cols={tile.cols || 1}>
            <img src={tile.img} alt={tile.title} />
          </GridListTile>
        ))}
      </GridList>
    </div>
  );
}

ImageGridList.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(ImageGridList);